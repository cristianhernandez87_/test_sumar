import React from 'react';

const Error = ({message}) => (
    <div className="alert alert-danger text-center">
        <p className="mb-0">{message}</p>
    </div>
)

export default Error;