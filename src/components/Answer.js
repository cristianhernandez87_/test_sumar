import React, { Fragment } from 'react';

const Answer = ({number, result}) => {
    return (
        <Fragment>
            <div className="row mt-5">
                <div className="card p-3 col-9 col-md-5 mx-auto text-center">
                    <p className="mb-3 text-bold alert alert-success">Numero inicial {number}</p>
                    <p className="mb-0">Resultado {result}</p>
                </div>
            </div>
        </Fragment>
    )
}

export default Answer;