import React, { useState } from 'react';

import Error from './Error';

const Form = ({saveNumber, saveResult, reloadShowanswer}) => {

    // st define
    const [ quantity, saveQuantity ] = useState(0);

    // st error
    const [ error, saveError ] = useState(false);

    // fn check number
    const defineQuantity = e => {
        saveQuantity( parseInt(e.target.value) );
    }
    
    // fn submit to define number
    const addQuantity = e => {
        e.preventDefault();

        // validate
        if( quantity === 0 || isNaN( quantity ) ) {
            saveError(true);
            return;
        }

        // if move from validate
        saveError(false);
        saveNumber(quantity);
        saveResult(quantity);
        reloadShowanswer(false);
    }


    return (
        <form
            className="col-11 col-md-6 card p-3 mx-auto"
            onSubmit={addQuantity}
        >

            <h3 className="w-100 text-center mb-3">Type a number</h3>

            { error ? <Error message="Agregar numero mayor o menor que 0"/> : null }

            <div className="form-group">
                <input
                    type="number"
                    className="form-control text-center"
                    placeholder="Put number"
                    onChange={defineQuantity}
                    value={quantity}
                />
            </div>
            <div className="form-group d-flex justify-content-between mb-0">
                <input
                    type="submit"
                    className="btn btn-primary col-5"
                    value="+"
                />
                <input
                    type="submit"
                    className="btn btn-danger col-5"
                    value="-"
                />
            </div>
        </form>
    );
}

export default Form;