import React, { Fragment, useState } from 'react';

import Form from './components/Form'
import Answer from './components/Answer'

function App() {

  // st define
  const [ number, saveNumber ] = useState(0);
  const [ result, saveResult ] = useState(0);
  const [ showanswer, reloadShowanswer ] = useState(true);

  return (
    <Fragment>
      <div className="container mt-5">
        <div className="row">
          <Form
            saveNumber={saveNumber}
            reloadShowanswer={reloadShowanswer}
            saveResult={saveResult}
           />
        </div>

        { showanswer 
          ? null 
          :  ( <Answer
                number={number}
                result={result}
            /> ) 
        }
      </div>
    </Fragment>
  );
}

export default App;
